<?php

/**
 * Created by PhpStorm.
 * User: Alexander_Malevich
 * Date: 02.09.2016
 * Time: 17:36
 */
namespace GalleryBundle\Manager;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AlbumManager
{
    private $encoder;
    private $normalizer;
    private $serializer;
    private $format;

    public function __construct($format)
    {
        $this->encoder = new JsonEncoder();
        $this->normalizer = new ObjectNormalizer();
        $this->normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer(array($this->normalizer), array($this->encoder));
        $this->format = $format;
    }

    public function serializeToJSON($albums)
    {
        return $this->serializer->serialize($albums, $this->format);
    }
}